package formative.fourteen.formative14.util;

public class FormatUtil {

    public static String formatCurrency(int amount) {
        return "Rp " + String.format("%,.0f", amount);
    }
}