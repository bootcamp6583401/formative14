package formative.fourteen.formative14.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.fourteen.formative14.models.SendingAddress;
import formative.fourteen.formative14.services.SendingAddressService;

@RestController
@RequestMapping("/addresses")
public class SendingAddressController {
    private final SendingAddressService sendingAddressService;

    @Autowired
    public SendingAddressController(
            SendingAddressService sendingAddressService) {
        this.sendingAddressService = sendingAddressService;
    }

    @GetMapping("/{id}")
    public SendingAddress getAddress(@PathVariable int id) {
        return sendingAddressService.getById(id);
    }
}
