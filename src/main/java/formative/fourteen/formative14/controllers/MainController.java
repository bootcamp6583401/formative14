package formative.fourteen.formative14.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import formative.fourteen.formative14.models.City;
import formative.fourteen.formative14.models.SendingAddress;
import formative.fourteen.formative14.services.CityService;
import formative.fourteen.formative14.services.CountryService;
import formative.fourteen.formative14.services.ProvinceService;
import formative.fourteen.formative14.services.SendingAddressService;

@Controller
@RequestMapping("/test")
public class MainController {
    private final CountryService countryService;
    private final ProvinceService provinceService;
    private final CityService cityService;
    private final SendingAddressService sendingAddressService;

    @Autowired
    public MainController(CountryService countryService, ProvinceService provinceService, CityService cityService,
            SendingAddressService sendingAddressService) {
        this.countryService = countryService;
        this.provinceService = provinceService;
        this.cityService = cityService;
        this.sendingAddressService = sendingAddressService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("countries", countryService.getAllCountries());
        model.addAttribute("provinces", provinceService.getAllProvinces());
        model.addAttribute("cities", cityService.getAllCities());
        return "index";
    }

    @PostMapping
    public String index(SendingAddress sendingAddress, @RequestParam int city_id) {
        System.out.println(sendingAddress);
        System.out.println("TESTINGG");
        System.out.println(sendingAddress.getId());

        City city = cityService.getById(city_id);

        System.out.println(city);

        sendingAddress.setCity(city);

        sendingAddressService.saveSendingAddress(sendingAddress);

        return "redirect:/test/addresses";
    }

    @GetMapping("/addresses")
    public String getList(Model model) {
        model.addAttribute("sendingAddresses", sendingAddressService.getAllSendingAddresss());

        return "list";
    }

    @PostMapping("/addresses/{id}")
    public String deleteAddress(@PathVariable int id) {
        sendingAddressService.deleteById(sendingAddressService.getById(id));
        return "redirect:/test/addresses";
    }
}
