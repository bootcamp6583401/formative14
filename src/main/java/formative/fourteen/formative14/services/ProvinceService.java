package formative.fourteen.formative14.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.fourteen.formative14.models.Province;
import formative.fourteen.formative14.repositories.ProvinceRepository;

@Service
public class ProvinceService {
    private final ProvinceRepository provinceRepository;

    @Autowired
    public ProvinceService(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public Iterable<Province> getAllProvinces() {
        return provinceRepository.findAll();
    }
}
