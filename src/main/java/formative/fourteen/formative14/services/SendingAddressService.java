package formative.fourteen.formative14.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.fourteen.formative14.models.SendingAddress;
import formative.fourteen.formative14.repositories.SendingAddressRepository;

@Service
public class SendingAddressService {
    private final SendingAddressRepository sendingAddressRepository;

    @Autowired
    public SendingAddressService(SendingAddressRepository sendingAddressRepository) {
        this.sendingAddressRepository = sendingAddressRepository;
    }

    public Iterable<SendingAddress> getAllSendingAddresss() {
        return sendingAddressRepository.findAll();
    }

    public SendingAddress saveSendingAddress(SendingAddress sendingAddress) {
        System.out.println("TESTINGG");
        System.out.println(sendingAddress);
        return sendingAddressRepository.save(sendingAddress);
    }

    public SendingAddress getById(int id) {
        return sendingAddressRepository.findById(id).get();
    }

    public void deleteById(SendingAddress sendingAddress) {
        sendingAddressRepository.delete(sendingAddress);
    }
}
