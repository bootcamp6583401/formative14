package formative.fourteen.formative14.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.fourteen.formative14.models.Country;
import formative.fourteen.formative14.repositories.CountryRepository;

@Service
public class CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public Iterable<Country> getAllCountries() {
        return countryRepository.findAll();
    }
}
