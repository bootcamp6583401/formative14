package formative.fourteen.formative14.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.fourteen.formative14.models.City;
import formative.fourteen.formative14.repositories.CityRepository;

@Service
public class CityService {
    private final CityRepository cityRepository;

    @Autowired
    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public Iterable<City> getAllCities() {
        return cityRepository.findAll();
    }

    public City getById(int id) {
        return cityRepository.findById(id).get();
    }
}
