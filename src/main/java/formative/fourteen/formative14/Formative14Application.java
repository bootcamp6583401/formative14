package formative.fourteen.formative14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative14Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative14Application.class, args);
	}

}
