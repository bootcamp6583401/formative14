package formative.fourteen.formative14.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.fourteen.formative14.models.Province;

public interface ProvinceRepository extends CrudRepository<Province, Integer> {
    List<Province> findByCountryId(int country_id);

}