package formative.fourteen.formative14.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.fourteen.formative14.models.SendingAddress;

public interface SendingAddressRepository extends CrudRepository<SendingAddress, Integer> {
    List<SendingAddress> findByCityId(int city_id);

}