package formative.fourteen.formative14.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.fourteen.formative14.models.District;

public interface DistrictRepository extends CrudRepository<District, Integer> {
    List<District> findByCityId(int city_id);

}