package formative.fourteen.formative14.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.fourteen.formative14.models.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {

}